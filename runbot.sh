#!/bin/bash
set -e -o pipefail

echo starting up at `date`
export HOME=/home/discobot
source venv/bin/activate
cd discord-reddit-feedbot
git pull
pip install -r requirements.txt
python -u main.py

