import pickle

class CommentSampler(object):
    def __init__(self, subreddit,submissions=False):
        self.subreddit = subreddit
        self.submissions = submissions
        if self.submissions:
            self.history_fn = 'submissions-'+subreddit.display_name+'.pickle'
        else:
            self.history_fn = 'comments-'+subreddit.display_name+'.pickle'
        try:
            with open(self.history_fn,'rb') as f:
                self.history = pickle.load(f)
        except FileNotFoundError:
            # no previous run - let's ignore everything before now
            self.history = list()
            self.get_new()
    
    def get_new(self):
        # get a new batch
        if self.submissions:
            newComments = list(self.subreddit.new())
        else:
            newComments = list(self.subreddit.comments())
        # filter out trash we've seen before
        newComments = [com for com in newComments if com.id not in self.history]
        
        # now add that to the things we've seen before
        ids = map(lambda x: x.id, newComments)
        self.history += ids
        
        # trim the excess
        if len(self.history)>300:
            self.history = self.history[-150:]
        with open(self.history_fn,'wb') as f:
            pickle.dump(self.history, f)
        
        # return those new things
        return newComments