from praw.models.reddit.submission import Submission

def summary(comment):
    if type(comment) is Submission:
        fmt = '{0.author} posted a new thread titled "{0.title}": {0.url}'
        return fmt.format(comment)
    else:    
        if comment.is_root:
            fmt = '{0.author} replied to {1.author}\'s thread "{2.title}": https://reddit.com{0.permalink}'
            parent = comment.submission
        else:
            fmt = '{0.author} replied to {1.author} in "{2.title}": https://reddit.com{0.permalink}?context=5'
            parent = comment.parent()
        return fmt.format(comment, parent, comment.submission)