import sys
import traceback
import discord
import praw
import asyncio
import configparser
from CommentSampler import CommentSampler
from CommentSummariser import summary
from utils import joinWithLimits

config = configparser.ConfigParser()
config.read('bot.ini')

reddit = praw.Reddit('bot1')

commentReaders = list()
for sub in [sect for sect in config.sections() if sect.startswith('sub')]:
    subname = config.get(sub,"name")
    subreddit = reddit.subreddit(subname)
    if(config.has_option(sub,"postchannel")):
        newChannel = config.getint(sub,"postchannel")
        commentReaders.append( (CommentSampler(subreddit, submissions=True), newChannel) )
        print("Watching posts on %s, posting to %s" % (subname, newChannel))
    if(config.has_option(sub,"commentchannel")):
        newChannel = config.getint(sub,"commentchannel")
        commentReaders.append( (CommentSampler(subreddit), newChannel) )
        print("Watching comments on %s, posting to %s" % (subname, newChannel))
if len(commentReaders) == 0:
    print("Config has no subreddits to monitor! Quitting.")
    exit(1)

client = discord.Client()

async def my_background_task():
    await client.wait_until_ready()
    while not client.is_closed():
        try:
            for reader in commentReaders:
                try:
                    newComms = reader[0].get_new()
                    if len(newComms) > 0:
                        summaries = list(map(summary, newComms))
                        summaries.reverse() # come in newest first, go out oldest first
                        # allegedly discord has a message limit of 2000, I'll aim a little lower
                        messages = joinWithLimits('\n', summaries, 1500)
                        for m in messages:
                            await client.get_channel(reader[1]).send(m)
                except:
                    print("Unexpected error:")
                    traceback.print_exception(*sys.exc_info())
                    print("Operating in sub/channel %s/%s" % (commentReaders[0][0].subreddit, commentReaders[0][1].id))
        except:
            print("Unexpected error:")
            traceback.print_exception(*sys.exc_info())
        await asyncio.sleep(30) # task runs every 60 seconds

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.loop.create_task(my_background_task())
print("starting discord")
client.run(config.get("bot","app_token"))
