def joinWithLimits(joiner, things, limit):
    buffer = things[0]
    outputs = []
    for t in things[1:]:
        if len(buffer)+len(t)+len(joiner) > limit:
            outputs.append(buffer)
            buffer=t
        else:
            buffer += joiner+t
    if len(buffer)>0:
        outputs.append(buffer)
    return outputs